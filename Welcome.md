"Hello everyone, I'm Ozgur SALGINCI, a Google Certified Cloud Architect with over 15 years of experience in software development, including mobile and full-stack technologies. Today, we're embarking on a unique, multi-faceted journey an open-source veterinary care system as a SaaS project, along with a mobile application for pet owners.

My primary aim is to share my extensive knowledge with the community, and I'm equally excited to learn from you all. To facilitate this collaborative learning, we're going to build an open-source veterinary care system as a SaaS project from the ground up. In parallel, we'll also be developing a mobile application designed for pet owners.


I want to clarify something important: while you don't need to be an expert programmer to follow along, this isn't a course that will teach you how to code. The focus here is on understanding complex architectures and how to leverage cloud technologies for scalable, efficient systems. I'll be sharing each module as we reach key milestones, but I won't be explaining every line of code.


This series is designed to take you from the initial steps, like purchasing a domain, all the way to deploying a fully functional project on the cloud. So, if you're interested in diving deep into architecture and cloud technologies, hit that subscribe button and follow our GitLab repository. Let's learn, build, and elevate our skills together."


